export const servicesDesignData1 = [
    { id: 1, heading: "Increases Brand Awareness & Recognition:", subHeading: "Cohesive and visually appealing design across your marketing materials strengthens your brand identity and increases brand recognition. People are more likely to remember and recall your brand when they encounter consistent design elements in your brochures, presentations, signage, or advertisements." },
    { id: 2, heading: "Boosts Audience Engagement & Drives Action:", subHeading: " Eye-catching visuals and clear messaging capture attention, spark interest, and encourage viewers to take action. Well-designed marketing materials can motivate potential customers to learn more about your offerings, visit your website, or make a purchase." },
    { id: 3, heading: "Generates Leads & Drives Sales: ", subHeading: "Compelling marketing materials can be a powerful lead generation tool. By effectively communicating your brand message and value proposition, you can attract potential customers and convert them into paying clients." },
    { id: 4, heading: "Strengthens Brand Credibility & Trust:", subHeading: " Professional design communicates professionalism and attention to detail, which can enhance brand credibility and build trust with potential customers. High-quality marketing materials make a positive first impression and position your brand as a leader in your industry." },
    { id: 5, heading: "Provides Measurable Results:", subHeading: " Many marketing materials, like email campaigns or social media ads, offer built-in analytics tools. By tracking key metrics, you can measure the effectiveness of your designs and make data-driven decisions to optimize your marketing efforts for better results" },
]

export const servicesDesignData2 = [
    { id: 1, heading: "Get a Free Quote:", subHeading: "Fill out our quick online form to receive a free quote and discuss your specific design needs with our team." },
    { id: 2, heading: "Browse Our Portfolio:", subHeading: "Explore our diverse portfolio of Business & Marketing Materials Design projects across various industries to see the impact we can create." },
    { id: 3, heading: "Download Our Design Guide:", subHeading: "Learn valuable tips and insights on crafting compelling marketing materials, the psychology of color in advertising, and designing for different print and digital formats." }
]