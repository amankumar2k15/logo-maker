export const servicesDesignData = [
    { id: 1, heading: "Expertise in Genre-Specific Design:", subHeading: " Our designers excel in crafting covers for any genre—be it fiction, non-fiction, children's books, or thrillers. With a keen eye for each genre's visual style, we guarantee your cover fits your book's category flawlessly." },
    { id: 2, heading: "Exceptional Attention to Detail:", subHeading: " We take pride in meticulous attention to detail, ensuring every element on your book cover design is polished and conveys the right message." },
    { id: 3, heading: "Focus on Reader Engagement:", subHeading: " We prioritize creating covers that are not only visually stunning but also designed to spark curiosity and compel readers to pick up your book." },
]

export const faqs = [
    { id: 1, ques: "What file formats do you provide for the final book cover design?", ans: " We provide your final book cover design in industry-standard formats suitable for both print and digital use (e.g., JPG, PNG, PSD)." },
    { id: 2, ques: " How long does the book cover design process typically take?", ans: "The design process typically takes 2-3 weeks, depending on the complexity of your project and the number of revisions required. However, we strive to deliver high-quality results efficiently while maintaining open communication throughout the process." },
    { id: 3, ques: "Do you offer Ebook Cover Design Services in addition to print book covers?", ans: "Absolutely! We offer a variety of branding packages that combine logo design with other services such as business card design, website design, and social media graphics. Feel free to contact us to discuss a custom package tailored to your specific needs." },
    { id: 4, ques: "I'm on a tight budget. Do you offer affordable book cover design options?", ans: "Getting started is easy! Simply visit our website and fill out a brief form outlining your brand and design preferences. A member of our team will then be in touch to discuss your project further and answer any questions you may have. " }
]

export const designProcess = [
    { id: 0, heading: "Initial Consultation", para: "We start with an in-depth conversation to understand your book's concept, target audience, and vision for the cover." },
    { id: 1, heading: "Concept Development", para: "Our designers will develop several unique book cover design concepts based on your input." },
    { id: 2, heading: "Revisions & Refinement", para: "We work closely with you throughout the process, incorporating your feedback and refining the design until it exceeds your expectations." },
    { id: 3, heading: "Final Delivery", para: "You'll receive your final book cover design in various formats suitable for print and digital use." },
]
