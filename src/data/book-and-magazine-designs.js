export const servicesDesignData1 = [
    { id: 1, heading: "Builds Recognition & Stands Out from the Crowd:", subHeading: "A well-designed cover or layout that reflects your genre and target audience makes your publication instantly recognizable on shelves or online marketplaces. Professional design helps you stand out from the competition and grab the attention of potential readers in a crowded space." },
    { id: 2, heading: "Communicates Genre & Theme Effectively:", subHeading: " The visual elements of your design, from the imagery to the typography, act as a visual cue for readers. A well-crafted design accurately communicates the genre, theme, and tone of your publication, giving readers a clear understanding of what to expect before they even open it." },
    { id: 3, heading: "Enhances Reader Engagement & Experience:", subHeading: " A visually appealing and user-friendly layout keeps readers engaged and immersed in your content. Professional design makes it easy for readers to navigate your publication, find the information they're looking for, and ultimately enjoy the reading experience." },
    { id: 4, heading: "Strengthens Brand Identity (for Magazines): ", subHeading: "For magazines, a consistent design style across all issues reinforces your brand identity and cultivates reader loyalty. A recognizable design becomes synonymous with your magazine, making it easier for readers to find and remember your publication." },
    { id: 5, heading: "Increases Sales & Conversions:", subHeading: " A captivating cover or layout can significantly increase the appeal of your publication, leading to more sales and downloads. Professional design entices potential readers to pick up your book or magazine, ultimately driving sales and conversions." },
]


export const servicesDesignData2 = [
    { id: 1, heading: "Get a Free Quote:", subHeading: " Fill out our quick online form to receive a free quote and discuss your specific design needs with our team." },
    { id: 2, heading: "Browse Our Portfolio:", subHeading: " Explore our diverse portfolio of Book & Magazine Design projects across various genres and formats to see the impact we can create." },
    { id: 3, heading: "Download Our Design Guide:", subHeading: " Learn valuable tips and insights on crafting captivating book covers, creating user-friendly magazine layouts, and designing for the digital age." },
]