export const priceCardData = [
    {
        id: 0, img: "/pricing/PricingBasicLogo.png", category: "Basic", para: " Unleash the Power of Your Business with Pro Plan.", money: "$350", translateOnHover: false, pricingList: [{
            id: "Basic", list: [
                "Logo design (3 variations)",
                "Business Card design",
                "Stationery design",
                "Website design",
                "Brochure/Flyer design",
                "High resolution image files",
                "Extend customization options",
                "After service adjustment(time bound)",
                "Dedicated support for design consultation"
            ],
        }]
    },
    {
        id: 1, img: "/pricing/PricingProBlack.png", category: "Popular", para: "Take Your Business to the Next Level with Business Plan.", money: "$540", translateOnHover: true, pricingList: [{
            id: "Popular", list: [
                "Logo design (2 variations)",
                "Business Card design",
                "Stationery design",
                "Website design",
                "Brochure/Flyer design",
                "High resolution image files",
                "Extend customization options",
                "After service adjustment(time bound)",
                "Dedicated support for design consultation"
            ],
        }]
    },
    {
        id: 2, img: "/pricing/PricingPro.png", category: "Pro", para: "Unleash the Power of Your Business with Pro Plan.", money: "$620", translateOnHover: false, pricingList: [{
            id: "Pro", list: [
                "Logo design (1 variations)",
                "Business Card design",
                "Stationery design",
                "Website design",
                "Brochure/Flyer design",
                "High resolution image files",
                "Extend customization options",
                "After service adjustment(time bound)",
                "Dedicated support for design consultation"
            ],
        }]
    },
]