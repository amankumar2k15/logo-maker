export const servicesDesignData = [
    { id: 1, heading: "Genre-Specific Design Expertise:", subHeading: " Our designers have extensive experience crafting ebook cover designs for a wide range of ebook genres, from fiction and non-fiction to romance, thrillers, and self-help books. We understand the design conventions of each genre and can create a cover that aligns perfectly with your ebook's category." },
    { id: 2, heading: "Conversion-Focused Design:", subHeading: " We go beyond aesthetics. We design ebook covers that are strategically crafted to convert viewers into buyers. We use eye-catching visuals, clear typography, and compelling titles to entice readers and encourage them to download your ebook." },
    { id: 3, heading: "High-Quality & Optimized Files:", subHeading: "  You'll receive your final ebook cover design in high-resolution formats optimized for various e-reader platforms (e.g., Kindle Direct Publishing, Apple iBooks), ensuring your cover looks sharp on any device." },
]

export const faqs = [
    { id: 1, ques: "Do you offer pre-made eBook cover templates that I can customize?", ans: " While we specialize in creating custom ebook cover designs tailored to your specific book, we may occasionally offer pre-made templates as a starting point. However, we highly recommend a custom design to ensure your ebook cover stands out from the competition." },
    { id: 2, ques: "What if I'm on a tight budget? Do you offer affordable ebook cover design services?", ans: "The design process typically takes 2-3 weeks, depending on the complexity of your project and the number of revisions required. However, we strive to deliver high-quality results efficiently while maintaining open communication throughout the process." },
    { id: 3, ques: "I have some ideas for my ebook cover design, can I share them with you?", ans: "Absolutely! We offer a variety of branding packages that combine logo design with other services such as business card design, website design, and social media graphics. Feel free to contact us to discuss a custom package tailored to your specific needs." },
    { id: 4, ques: "How long does the ebook cover design process typically take?", ans: "Getting started is easy! Simply visit our website and fill out a brief form outlining your brand and design preferences. A member of our team will then be in touch to discuss your project further and answer any questions you may have. " }
]

export const designProcess = [
    { id: 0, heading: "Initial Consultation", para: "We start with an in-depth conversation to understand your ebook's concept, target audience, and vision for the cover." },
    { id: 1, heading: "Content & Style Analysis", para: "Our designers will develop several unique ebook cover design concepts based on your input." },
    { id: 2, heading: "Concept Development", para: "We work closely with you throughout the process, incorporating your feedback and refining the design until it exceeds your expectations." },
    { id: 3, heading: "Revisions & Refinement", para: "We work closely with you throughout the process, incorporating your feedback and refining the design until it exceeds your expectations." },
    { id: 4, heading: "Final Delivery", para: "You'll receive your final ebook cover design files in various formats optimized for different e-reader platforms." },
]
