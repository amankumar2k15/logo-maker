import tellYourStory1 from "../animate/tellYourStory1.json";
import tellYourStory2 from "../animate/tellYourStory2.json";
import tellYourStory3 from "../animate/tellYourStory3.json";
import tellYourStory4 from "../animate/tellYourStory4.json";
import tellYourStory5 from "../animate/tellYourStory5.json";
import tellYourStory6 from "../animate/tellYourStory6.json";

//Home > tellStoryContent
export const tellStoryContent = [
    { lottieImg: tellYourStory1, step: 1, heading: "Tell Your Story", para: "Share your brand vision, inspiration, and any specific ideas you may have. The more we know about your brand, the better we can capture its essence in your logo." },
    { lottieImg: tellYourStory2, step: 2, heading: "Connect with a Designer", para: "Schedule a convenient video call with one of our talented designers. This initial conversation allows you to discuss your brand vision in detail and ask any questions you may have. Our designer will actively listen to your ideas and gain a deep understanding of your brand's unique personality." },
    { lottieImg: tellYourStory3, step: 3, heading: "Refine Your Vision", para: "After the video call, our designer will present initial logo concepts based on your discussions. This collaborative stage allows you to provide feedback and suggestions. We encourage open communication to ensure the logo aligns perfectly with your brand identity." },
    { lottieImg: tellYourStory4, step: 4, heading: "Design Magic Happens", para: "Once you've approved a concept direction, our designer will take center stage and bring your vision to life. They will meticulously craft your logo, paying close attention to detail and ensuring it resonates with your target audience." },
    { lottieImg: tellYourStory5, step: 5, heading: "Unlimited Revisions", para: "We understand that finding the perfect logo is an iterative process. That's why we offer unlimited revisions throughout the design phase. You can provide feedback on colors, fonts, and overall layout until you're completely satisfied with the final product." },
    { lottieImg: tellYourStory6, step: 6, heading: "Download & Brand with Confidence", para: "Upon your final approval, you'll receive your logo in various formats suitable for print and digital use. With your brand identity firmly established, you can now confidently launch your business or elevate your existing brand image." },
]

//Home > HeroBanner 
export const Card = [
    { id: 1, img: "/home/Card1.png" },
    { id: 2, img: "/home/Card2.png" },
    { id: 3, img: "/home/Card3.png" },
    { id: 4, img: "/home/Card4.png" },
    { id: 5, img: "/home/Card5.png" },
    { id: 6, img: "/home/Card1.png" },
    { id: 7, img: "/home/Card2.png" },
    { id: 8, img: "/home/Card3.png" },
    { id: 9, img: "/home/Card4.png" },
    { id: 10, img: "/home/Card5.png" }
]

//Home > HomeSlider
export const BoxData = [
    { id: 1, image: "/home/Service1.png", textPosition: "text-center", justifyContent: "justify-center", alignItems: "items-center", headingText: "Personalized approach", paraText: "We closely collaborate with you to ensure your logo reflects your brand identity flawlessly, every step of the way." },
    { id: 2, image: "/home/Service2.png", textPosition: "text-center", justifyContent: "justify-center", alignItems: "items-center", headingText: "Experienced designers", paraText: "With expertise spanning various industries, our team excels in logo design, bringing extensive experience to every project." },
    { id: 3, image: "/home/Service3.png", textPosition: "text-center", justifyContent: "justify-center", alignItems: "items-center", headingText: "Collaborative process", paraText: "We collaborate closely with you, incorporating your feedback at every step for a tailored design experience." },
    { id: 4, image: "/home/Service4.png", textPosition: "text-center", justifyContent: "justify-center", alignItems: "items-center", headingText: "Unlimited revisions", paraText: "We get it, finding the perfect logo takes time. That's why we offer unlimited revisions for your complete satisfaction." },
    { id: 5, image: "/home/Service5.png", textPosition: "text-center", justifyContent: "justify-center", alignItems: "items-center", headingText: "Fast turnaround times", paraText: "Don't wait endlessly for your dream logo. Our efficient process ensures high-quality results delivered promptly." },
    { id: 6, image: "/home/Service6.png", textPosition: "text-center", justifyContent: "justify-center", alignItems: "items-center", headingText: "Affordable pricing", paraText: "We offer flexible and competitive pricing packages designed to fit any budget without compromising quality." }
]

//Home > FAQ
export const faqs = [
    { id: 1, ques: "What are the benefits of using a human designer over an AI logo maker?", ans: "Human designers understand the importance of brand strategy and can create logos that are not only visually appealing but also strategically positioned to resonate with your target audience. AI logo makers lack the ability to grasp the nuances of your brand and create logos that are truly unique and meaningful." },
    { id: 2, ques: "How long does the logo design process typically take?", ans: "The design process typically takes 2-3 weeks, depending on the complexity of your project and the number of revisions required. However, we strive to deliver high-quality results efficiently while maintaining open communication throughout the process." },
    { id: 3, ques: "Do you offer logo design packages that include other branding materials?", ans: "Absolutely! We offer a variety of branding packages that combine logo design with other services such as business card design, website design, and social media graphics. Feel free to contact us to discuss a custom package tailored to your specific needs." },
    { id: 4, ques: "How can I get started?", ans: "Getting started is easy! Simply visit our website and fill out a brief form outlining your brand and design preferences. A member of our team will then be in touch to discuss your project further and answer any questions you may have. " }
]



//footer 
export const footerLinks = [
    {
        heading: "Category", data: [
            {
                id: 1,
                heading: "Book and magazine designs",
                url: "/services/book-and-magazine-designs"
            },
            {
                id: 2,
                heading: "Clothing and Merchandise",
                url: "/services/clothing-and-merchandise"
            },
            {
                id: 3,
                heading: "Illustration and Art",
                url: "/services/illustration-and-art"
            },
            {
                id: 4,
                heading: "Social Media Design",
                url: "/services/social-media-design"
            },
            {
                id: 5,
                heading: "Packaging and Label designs",
                url: "/services/packaging-and-label-designs"
            },
            {
                id: 6,
                heading: "Website and App Design",
                url: "/services/website-and-app-design"
            },
            {
                id: 7,
                heading: "Brand And Identity Design",
                url: "/services/brand-and-identity-design"
            },
            {
                id: 8,
                heading: "Business and advertising designs",
                url: "/services/business-and-advertising-designs"
            },
        ]
    },
    {
        heading: "Get design", data: [
            { heading: "Logo design" },
            { heading: "Business cards" },
            { heading: "Product packaging" },
            { heading: "Landing page design" },
            { heading: "website development" },
            { heading: "Social media profile banners" },
            { heading: "Custom illustrations" }
        ]
    },
    {
        heading: "Support", data: [
            { heading: "Help Desk", },
            { heading: "Disclaimer", },
            { heading: "Privacy Policy" },
            { heading: "Terms  and Conditions" }
        ]
    },
    {
        heading: "Company", data: [
            { heading: "About us ", url: "/about-us" },
            { heading: "Pricing", url: "/price" },
            { heading: "Contact us ", url: "/contact" }
        ]
    },
    {
        heading: "Resources", data: [
            { heading: "Documentation" },
            { heading: "Papers" },
            { heading: "Press Conferences" }
        ]
    }
]

//footer 
export const socialLinks = [
    { img: "Facebook", },
    { img: "Insta", },
    { img: "Twitter", },
    { img: "Lknd", },
    { img: "Pint", }
]


// navbar 
export const dropMenu = [
    {
        id: 1,
        img: "/navbar/navbarBook.png",
        heading: "Book and magazine designs",
        subHeading: "Stunning designs that perform",
        url: "book-and-magazine-designs"
    },
    {
        id: 2,
        img: "/navbar/navbarClothes.png",
        heading: "Clothing and Merchandise",
        subHeading: "All about how to use",
        url: "clothing-and-merchandise"
    },
    {
        id: 3,
        img: "/navbar/navbarIllustration.png",
        heading: "Illustration and Art",
        subHeading: "Newest feature releases",
        url: "illustration-and-art"
    },
    {
        id: 4,
        img: "/navbar/navbarSocialMedia.png",
        heading: "Social Media Design",
        subHeading: "Immersive assets for ever screen",
        url: "social-media-design"
    },
]

// navbar 
export const dropMenus = [
    {
        id: 1,
        img: "/navbar/navbarPackaging.png",
        heading: "Packaging and Label designs",
        subHeading: "Everything you need to know",
        url: "packaging-and-label-designs"
    },
    {
        id: 2,
        img: "/navbar/navbarWebsiteApp.png",
        heading: "Website and App Design",
        subHeading: "How our products help you",
        url: "website-and-app-design"
    },
    {
        id: 3,
        img: "/navbar/navbarBrand.png",
        heading: "Brand And Identity Design",
        subHeading: "Building products and systems",
        url: "brand-and-identity-design"
    },
    {
        id: 4,
        img: "/navbar/navbarBook.png",
        heading: "Business and advertising designs",
        subHeading: "Discover books & webinars",
        url: "business-and-advertising-designs"
    },

]


//About > HomeSlider
export const BoxAboutData = [
    { id: 1, image: "/home/Service1.png", textPosition: "text-center", justifyContent: "justify-center", alignItems: "items-center", headingText: "Personalized approach", paraText: "We closely collaborate with you to ensure your logo reflects your brand identity flawlessly, every step of the way." },
    { id: 2, image: "/home/Service2.png", textPosition: "text-center", justifyContent: "justify-center", alignItems: "items-center", headingText: "Experienced designers", paraText: "With expertise spanning various industries, our team excels in logo design, bringing extensive experience to every project." },
    { id: 3, image: "/home/Service3.png", textPosition: "text-center", justifyContent: "justify-center", alignItems: "items-center", headingText: "Collaborative process", paraText: "We collaborate closely with you, incorporating your feedback at every step for a tailored design experience." },
]

//About > FAQ 
export const AboutFaqs = [
    { id: 1, ques: "Unveiling Your Brand Vision", ans: "Through in-depth conversations and creative brainstorming, we delve into your brand story, target audience, and the values you represent." },
    { id: 2, ques: "Understanding Your Competitive Landscape", ans: "Through in-depth conversations and creative brainstorming, we delve into your brand story, target audience, and the values you represent." },
    { id: 3, ques: "Strategic Design Decisions", ans: "Through in-depth conversations and creative brainstorming, we delve into your brand story, target audience, and the values you represent." },
]



// heroBanner > search data 
export const searchData = [
    { name: 'Brand Personality', url: "category/brand-personality" },
    { name: 'Magazine Design', url: "category/magazine-design" },
    { name: 'Book Cover Design', url: "category/book-cover-design" },
    { name: 'EBook Cover Design', url: "category/ebook-cover-design" },

    { name: 'Business & Advertising', url: "category/business-&-advertising" },
    { name: 'PowerPoint Design', url: "category/powerpoint-design" },
    { name: 'Poster Design', url: "category/poster-design" },
    { name: 'Catalog Design', url: "category/catalog-design" },
    { name: 'Leaflet Design', url: "category/leaflet-design" },
    { name: 'Newspaper Ad Design', url: "category/newspaper-ad-design" },
    { name: 'Advertisement Design', url: "category/advertisement-design" },
    { name: 'Newsletter Design', url: "category/newsletter-design" },

    { name: 'Menu Design', url: "category/menu-design" },
    { name: 'Calendar Design', url: "category/calendar-design" },
    { name: 'Brochure Design', url: "category/brochure-design" },
    { name: 'Infographic Design', url: "category/infographic-design" },
    { name: 'Email Design', url: "category/email-design" },
    { name: 'Signage Design', url: "category/signage-design" },
    { name: 'Billboard Design', url: "category/billboard-design" },
    { name: 'Car, Truck or Van Wrap Design', url: "category/car,-truck-or-van-wrap-design" },
    { name: 'Trade Show Banner Design', url: "category/trade-show-banner-design" },
    { name: 'Trade Show Booth Design', url: "category/trade-show-booth-design" },
    { name: 'Postcard Design', url: "category/postcard-design" },
    { name: 'Print Flyer Design', url: "category/print-flyer-design" },

    { name: 'Clothing & Merchandise', url: "category/clothing-&-merchandise" },
    { name: 'Sticker Design', url: "category/sticker-design" },
    { name: 'T-Shirt Design', url: "category/t-shirt-design" },
    { name: 'Merchandise Design', url: "category/merchandise-design" },
    { name: 'Personalized Cup or Mug Design', url: "category/personalized-cup-or-mug-design" },
    { name: 'Bag & Tote Design', url: "category/bag-&-tote-design" },
    { name: 'Hat & Cap Design', url: "category/hat-&-cap-design" },

    { name: 'Illustration & Art', url: "category/illustration-&-art" },
    { name: 'Card Design or Invitation Design', url: "category/card-design-or-invitation-design" },
    { name: 'Photo Manipulation & Design', url: "category/photo-manipulation-&-design" },
    { name: 'Illustration & Graphics', url: "category/illustration-&-graphics" },
    { name: 'Character & Mascot Design', url: "category/character-&-mascot-design" },
    { name: 'Tattoo Designs', url: "category/tattoo-designs" },

    { name: 'Social Media Design', url: "category/social-media-design" },
    { name: 'Twitter Header Design', url: "category/twitter-header-design" },
    { name: 'Facebook Cover Design', url: "category/facebook-cover-design" },
    { name: 'Social Media Post Design', url: "category/social-media-post-design" },
    { name: 'LinkedIn Cover Design', url: "category/linkedin-cover-design" },

    { name: 'Packaging & Label Design', url: "category/packaging-&-label-design" },
    { name: 'Packaging Design', url: "category/packaging-design" },
    { name: 'Label Design', url: "category/label-design" },
    { name: 'Mobile Cover Design', url: "category/mobile-cover-design" },
    { name: 'CD Cover Design', url: "category/cd-cover-design" },

    { name: 'Website & App Design', url: "category/website-&-app-design" },
    { name: 'Banner Ad Design', url: "category/banner-ad-design" },
    { name: 'Blog Design', url: "category/blog-design" },
    { name: 'App Icon or Button Design', url: "category/app-icon-or-button-design" },
    { name: 'Mobile App Design', url: "category/mobile-app-design" },
    { name: 'Website Design', url: "category/website-design" },
    { name: 'Landing Page Design', url: "category/landing-page-design" },

    { name: 'Brand & Identity Design', url: "category/brand-&-identity-design" },
    { name: 'Stationery Design', url: "category/stationery-design" },
    { name: 'Letterhead Design', url: "category/letterhead-design" },
    { name: 'Envelope Design', url: "category/envelope-design" },
    { name: 'Logo Design', url: "category/logo-design" },
    { name: 'Business Card Design', url: "category/business-card-design" },
    { name: 'Logo & Business Card Design', url: "category/logo-&-business-card-design" },
    { name: 'Logo & Brand Identity Design', url: "category/logo-&-brand-identity-design" }
];

