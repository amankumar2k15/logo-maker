//Services > FAQ
export const faqs = [
    { id: 1, ques: "What are the benefits of using a human designer over an AI logo maker?", ans: "Human designers understand the importance of brand strategy and can create logos that are not only visually appealing but also strategically positioned to resonate with your target audience. AI logo makers lack the ability to grasp the nuances of your brand and create logos that are truly unique and meaningful." },
    { id: 2, ques: "How long does the logo design process typically take?", ans: "The design process typically takes 2-3 weeks, depending on the complexity of your project and the number of revisions required. However, we strive to deliver high-quality results efficiently while maintaining open communication throughout the process." },
    { id: 3, ques: "Do you offer logo design packages that include other branding materials?", ans: "Absolutely! We offer a variety of branding packages that combine logo design with other services such as business card design, website design, and social media graphics. Feel free to contact us to discuss a custom package tailored to your specific needs." },
    { id: 4, ques: "How can I get started?", ans: "Getting started is easy! Simply visit our website and fill out a brief form outlining your brand and design preferences. A member of our team will then be in touch to discuss your project further and answer any questions you may have. " }
]


export const servicesDesignData = [
    { id: 1, heading: "Get a Free Quote: ", subHeading: " Fill out our quick online form to receive a free quote and discuss your specific website or app design needs with our team." },
    { id: 2, heading: "Browse Our Portfolio: ", subHeading: " Explore our diverse portfolio of Website & App Design projects across various industries to see the impact we can create." },
    { id: 3, heading: "Download Our Design Guide: ", subHeading: " Learn valuable tips and insights on crafting user-centric websites and apps that drive engagement and achieve your business goals." },
]