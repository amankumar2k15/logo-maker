//Services > FAQ
export const faqs = [
    { id: 1, ques: "What are the benefits of using a human designer over an AI logo maker?", ans: "Human designers understand the importance of brand strategy and can create logos that are not only visually appealing but also strategically positioned to resonate with your target audience. AI logo makers lack the ability to grasp the nuances of your brand and create logos that are truly unique and meaningful." },
    { id: 2, ques: "How long does the logo design process typically take?", ans: "The design process typically takes 2-3 weeks, depending on the complexity of your project and the number of revisions required. However, we strive to deliver high-quality results efficiently while maintaining open communication throughout the process." },
    { id: 3, ques: "Do you offer logo design packages that include other branding materials?", ans: "Absolutely! We offer a variety of branding packages that combine logo design with other services such as business card design, website design, and social media graphics. Feel free to contact us to discuss a custom package tailored to your specific needs." },
    { id: 4, ques: "How can I get started?", ans: "Getting started is easy! Simply visit our website and fill out a brief form outlining your brand and design preferences. A member of our team will then be in touch to discuss your project further and answer any questions you may have. " }
]

export const servicesDesignData1 = [
    { id: 1, heading: "Builds Brand Recognition & Memorability:", subHeading: "A cohesive brand identity makes your company instantly recognizable across all touchpoints, from your website and social media to marketing materials and packaging." },
    { id: 2, heading: "Creates Trust & Credibility:", subHeading: "A professional and consistent brand image instills trust and confidence in your target audience." },
    { id: 3, heading: "Download Our Design Guide:", subHeading: "Learn more about the importance of brand identity and the benefits of professional design services." },
    { id: 4, heading: "Differentiates You from the Competition:", subHeading: "A unique brand identity sets you apart from your competitors, making a lasting impression on potential customers." },
    { id: 5, heading: "Empowers Brand Storytelling:", subHeading: "Your brand identity serves as the visual language that breathes life into your brand story, connecting with customers on a deeper level." },
    { id: 6, heading: "Strengthens Employee Engagement:", subHeading: "A strong brand identity fosters a sense of pride and ownership among your employees, aligning them with your company's core values." },
]

export const servicesDesignData2 = [
    { id: 1, heading: "Get a Free Quote:", subHeading: " Let's discuss your brand identity needs! Fill out our quick online form to receive a free quote and consultation." },
    { id: 2, heading: "Browse Our Portfolio:", subHeading: " Browse Our Portfolio: Get inspired by exploring our diverse portfolio of branding projects across various industries." },
    { id: 3, heading: "Download Our Design Guide:", subHeading: " Learn more about the importance of brand identity and the benefits of professional design services." },
]



 

