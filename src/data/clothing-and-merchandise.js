export const servicesDesignData1 = [
    { id: 1, heading: "Builds Brand Recognition & Memorability:", subHeading: "Custom clothing and merchandise act as walking billboards for your brand. A cohesive design aesthetic across your merchandise line reinforces your brand identity and increases brand recognition wherever your products are seen. People wearing or using your merchandise become brand ambassadors, spreading brand awareness through their everyday activities." },
    { id: 2, heading: "Strengthens Brand Loyalty & Community: ", subHeading: "Strengthens Brand Loyalty & Community: Custom clothing and merchandise can foster a sense of community and belonging among your customers or supporters. Wearing or using branded items allows people to identify with your brand and connect with others who share similar interests. This sense of community can strengthen brand loyalty and create a lasting connection with your audience" },
    { id: 3, heading: "Drives Sales & Conversions: ", subHeading: "Eye-catching and well-designed merchandise can be a powerful sales tool. Attractive products entice customers to purchase and promote your brand, leading to increased sales and conversions." },
    { id: 4, heading: "Offers a Unique Marketing Channel: ", subHeading: "Custom clothing and merchandise can be a versatile marketing tool. Promotional items like t-shirts, tote bags, or stickers can be used for giveaways, event merchandise, or product bundling, creating a unique touchpoint for potential customers to interact with your brand." },
    { id: 5, heading: "Provides Opportunities for Self-Expression:", subHeading: "Custom clothing and merchandise can be a fun way to express your unique style and personality. By designing your own items, you can showcase your creativity and interests through the clothing and accessories you wear." }
]


export const servicesDesignData2 = [
    { id: 1, heading: "Get a Free Quote:", subHeading: " Fill out our quick online form to receive a free quote and discuss your specific design needs with our team." },
    { id: 2, heading: "Browse Our Portfolio:", subHeading: " Explore our diverse portfolio of Clothing & Merchandise Design projects across various industries and styles to see the impact we can create." },
    { id: 3, heading: "Download Our Design Guide:", subHeading: " Learn valuable tips and insights on crafting impactful t-shirt designs, the psychology of color in clothing design, and creating a cohesive merchandise line for your brand." },
]