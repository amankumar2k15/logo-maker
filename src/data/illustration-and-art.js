export const servicesDesignData1 = [
    { id: 1, heading: "Builds Brand Recognition & Memorability:", subHeading: "Consistent, captivating illustrations strengthen brand identity, fostering instant recognition and recall. They create a memorable visual signature, ensuring your brand stands out in the minds of your audience." },
    { id: 2, heading: "Creates Trust & Credibility:", subHeading: "Art evokes emotions and deepens audience connection. Compelling visuals tap into human emotions, nurturing loyalty and advocacy. Aligning illustrations with your audience builds a stronger emotional bond with your brand." },
    { id: 3, heading: "Differentiates You from the Competition:", subHeading: "Illustrations tell stories uniquely, visually portraying your brand's narrative, values, and message unlike text alone. Crafted well, they encapsulate your brand essence, communicating it memorably and engagingly." },
    { id: 4, heading: "Empowers Brand Storytelling: ", subHeading: "Unique, visually compelling illustrations distinguish your brand in a crowded market. Our artists craft attention-grabbing visuals that spark curiosity and make a lasting impression, setting you apart from competitors and establishing a distinctive brand presence." },
    { id: 5, heading: "Strengthens Employee Engagement:", subHeading: "Striking illustrations boost engagement across marketing materials, websites, and social media. Compelling visuals seize attention, halt scrolling, and prompt users to explore your brand further, driving conversions and desired actions." },
]


export const servicesDesignData2 = [
    { id: 1, heading: "Get a Free Quote:", subHeading: "  Fill out our quick online form to receive a free quote." },
    { id: 2, heading: "Browse Our Portfolio:", subHeading: " Explore our diverse portfolio of Illustration & Art projects across various industries to see the impact we can create." },
    { id: 3, heading: "Download Our Design Guide:", subHeading: " Learn valuable tips and insights on the power of illustration and art in branding, character design principles, and the art of visual storytelling." },
]