export const servicesDesignData = [
    { id: 1, heading: "Expertise in Diverse Magazine Genres:", subHeading: " Our seasoned designers have a knack for creating award-winning magazine designs across genres like fashion, business, sports, lifestyle, and more. We get each genre's vibe, so your design always fits your magazine's style" },
    { id: 2, heading: "Enhanced Readability & User Experience:", subHeading: " We prioritize layouts that are not only visually appealing but also optimized for user experience. We ensure clear hierarchy, easy-to-read fonts, and strategically placed images to guide readers through your content and keep them engaged." },
    { id: 3, heading: "Customized Design Solutions:", subHeading: " We take the time to understand your unique vision and target audience. We don't offer generic templates - we create magazine designs that are tailored to your specific needs and brand identity." },
]

export const faqs = [
    { id: 1, ques: "What software do you use for magazine design?", ans: "We use industry-standard design software such as Adobe InDesign, ensuring your magazine design files are compatible with professional printing and publishing workflows." },
    { id: 2, ques: "Can you help with magazine cover design as a standalone service?", ans: "The design process typically takes 2-3 weeks, depending on the complexity of your project and the number of revisions required. However, we strive to deliver high-quality results efficiently while maintaining open communication throughout the process." },
    { id: 3, ques: "Do you offer services for designing magazines for online publication?", ans: "Absolutely! We offer a variety of branding packages that combine logo design with other services such as business card design, website design, and social media graphics. Feel free to contact us to discuss a custom package tailored to your specific needs." },
    { id: 4, ques: "How long does the magazine design process typically take?", ans: "Getting started is easy! Simply visit our website and fill out a brief form outlining your brand and design preferences. A member of our team will then be in touch to discuss your project further and answer any questions you may have. " }
]

export const designProcess = [
    { id: 0, heading: "Initial Consultation", para: "We start with an in-depth conversation to understand your magazine's concept, target audience, and vision for the design." },
    { id: 1, heading: "Content & Style Analysis", para: "We analyze your content and desired aesthetic to develop a design strategy that complements your voice and message." },
    { id: 2, heading: "Concept Development", para: "Our designers will develop several unique magazine design concepts based on your input." },
    { id: 3, heading: "Revisions & Refinement", para: "We work closely with you throughout the process, incorporating your feedback and refining the design until it exceeds your expectations." },
    { id: 4, heading: "Final Delivery", para: "You'll receive your final magazine design files, including layouts, covers, and templates, ready for print or digital publishing." },
]