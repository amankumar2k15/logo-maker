export const servicesDesignData1 = [
    { id: 1, heading: "Builds Brand Recognition & Memorability:", subHeading: "A unified and attractive design on your packaging and labels strengthens your brand identity, ensuring instant recognition on shelves. Each interaction becomes a branding chance, enhancing recall and nurturing lasting customer connections." },
    { id: 2, heading: "Creates Trust & Credibility:", subHeading: "  High-quality packaging and labeling with clear information instills trust and confidence in potential customers. Professional design communicates attention to detail and quality, encouraging consumers to choose your product over the competition." },
    { id: 3, heading: "Differentiates You from the Competition:", subHeading: " In a crowded marketplace, unique and visually compelling packaging and labels can set your product apart. Our design expertise will help you create packaging that grabs attention, sparks interest, and leaves a lasting impression on potential buyers." },
    { id: 4, heading: "Empowers Brand Storytelling:", subHeading: " Packaging and labeling are potent storytelling tools. Visuals, materials, and product descriptions unite to convey your brand story and values. A captivating design resonates emotionally with customers, nurturing brand loyalty and advocacy." },
    { id: 5, heading: "Strengthens Employee Engagement:", subHeading: " Packaging and labeling aim to attract customers to choose your product. Eye-catching visuals, clear messaging, and a call to action contribute to increased sales. Effective communication of your product's value proposition encourages purchases." },
]

export const servicesDesignData2 = [
    { id: 1, heading: "Get a Free Quote:", subHeading: " Fill out our quick online form to receive a free quote and discuss your specific design needs with our team." },
    { id: 2, heading: "Browse Our Portfolio:", subHeading: " Explore our diverse portfolio of Packaging & Labeling Design projects across various industries to see the impact we can create." },
    { id: 3, heading: "Download Our Design Guide:", subHeading: " Learn valuable tips and insights on crafting effective packaging design, the psychology of color in packaging, and designing labels that comply with regulations." },
]
