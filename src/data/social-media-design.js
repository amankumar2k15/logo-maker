export const servicesDesignData1 = [
    { id: 1, heading: "Builds Brand Recognition & Memorability:", subHeading: "Consistent, visually appealing design style across all social platforms enhances brand identity, making your content instantly recognizable and reinforcing brand recall with your audience." },
    { id: 2, heading: "Creates Trust & Credibility:", subHeading: "Professional social media visuals convey attention to detail and brand credibility, building trust and encouraging audience engagement and consideration." },
    { id: 3, heading: "Differentiates You from the Competition:", subHeading: "In a crowded social media scene, stand out with unique, visually compelling content. Our design expertise ensures attention-grabbing, curiosity-sparking creations that leave a lasting impression, setting you apart from competitors." },
    { id: 4, heading: "Empowers Brand Storytelling: ", subHeading: "Your social media visuals tell compelling stories, conveying your brand's story, values, and personality. Captivating narratives resonate emotionally, fostering loyalty and advocacy." },
    { id: 5, heading: "Strengthens Employee Engagement:", subHeading: "Social media aims to ignite discussions, forge connections, and prompt actions. Compelling visuals and engaging content drive higher engagement, website traffic, and conversions, be it purchases, sign-ups, or downloads." },
]


export const servicesDesignData2 = [
    { id: 1, heading: "Get a Free Quote:", subHeading: " Fill out our quick online form to receive a free quote and discuss your specific social media design needs with our team." },
    { id: 2, heading: "Browse Our Portfolio:", subHeading: " Explore our diverse portfolio of Social Media Design projects across various industries to see the impact we can create." },
    { id: 3, heading: "Download Our Design Guide:", subHeading: "  Learn valuable tips and insights on crafting a winning social media strategy, leveraging the power of visuals, and maximizing your social media engagement." },
]